#!/usr/bin/python3
import subprocess
import threading
import os
numfiles = 1

def writeFile(host, filename):
   subprocess.run(["xrdcp", "-f", "/tmp/testfile", "root://" + host + "//" + filename])
   
def tpc(src, dest, filename):
   subprocess.run(["xrdcp", "--tpc only", "-f", "root://" + host + "//" + filename, "root://" + host + "//" + filename])
    

print ("Running copy tests and testing symlink creation")
pfns = [ ]
hosts = []
hosts.append("192.168.122.200:1094")
hosts.append("192.168.122.210:1094")
hosts.append("192.168.122.211:1094")
hosts.append("192.168.122.212:1094")
hosts.append("192.168.122.220:1094")
hosts.append("192.168.122.221:1094")
hosts.append("192.168.122.222:1094")
writeThreads = []
deleteThreads = []
counter = 0
for host in hosts:
        filename = "autoTest" + str(counter)
        x = threading.Thread(target=writeFile, args=(host,filename))
        writeThreads.append(x)
        counter+=1
for thread in writeThreads:
   thread.start()
for thread in writeThreads:
   thread.join()

tpc("192.168.122.210", "192.168.122.220", "autoTest1")
tpc("192.168.122.210", "192.168.122.220", "autoTest2")
tpc("192.168.122.220", "192.168.122.210", "autoTest3")
tpc("192.168.122.220", "192.168.122.210", "autoTest4")
#xrdcp --tpc only root://192.168.122.220:1094//testfile root://192.168.122.210:1094//testfile1003
