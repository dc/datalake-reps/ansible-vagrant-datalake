from kafka import KafkaConsumer
import json

consumer = KafkaConsumer(
    "numtest",
    bootstrap_servers='192.168.122.214:9092',
    auto_offset_reset='earliest',
    group_id="consumer-group-replicator")
print("Starting consumer")
for msg in consumer:
    print("Received: ", msg.value)


#######################################

