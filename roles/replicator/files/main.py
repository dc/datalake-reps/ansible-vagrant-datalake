import subprocess
import matplotlib.pyplot as plt

CONST_DESTINATIONS = ['192.168.122.200', '192.168.122.211', '192.168.122.212', '192.168.122.221', '192.168.122.222']
FILE_TO_COPY = "tst1"
data_server = {'192.168.122.210': 0, '192.168.122.220': 0}


def contains_error_last_line(text):
    status_of_execution = "ordinary"
    for line in reversed(text.splitlines()):
        if not line:
            continue
        else:
            status_of_execution = line
            break
    if "error" in status_of_execution.lower():
        return True, status_of_execution
    return False, status_of_execution


def execute_xrdcp(file_name, ip):
    try:
        dst = "root://" + ip + "//" + file_name
        proc = subprocess.Popen(["xrdcp", file_name, dst, "--debug", "1"], stderr=subprocess.PIPE)
        stderr = proc.communicate()[1]
        return stderr.decode('utf-8')
    except OSError as err:
        print("OS error: {0}".format(err))
        quit()


def validate_result(std_err, ip):
    failed, error_line = contains_error_last_line(std_err)
    if failed:
        print("xrdcp failed: ", error_line)
    else:
        print("xrdcp successful, file copied to: " + ip)
    return failed


def remove_copy(file_name):
    path_to_file = "/" + file_name
    try:
        for ip in data_server:
            target = "root://" + ip + ":1094"
            proc = subprocess.Popen(["xrdfs", target, "rm", path_to_file], stderr=subprocess.PIPE)
            stderr = proc.communicate()[1]
            if not stderr:
                print("deleted copy at: " + ip)
                data_server[ip] += 1
    except OSError as err:
        print("OS error: {0}".format(err))
        quit()


def generate_bar_chart(amount_of_tries):
    plt.bar(range(len(data_server)), list(data_server.values()), align='center')
    plt.xticks(range(len(data_server)), list(data_server.keys()))
    plt.xlabel("Data-Server")
    plt.ylabel("Copies")
    plt.title(str(amount_of_tries) + " copy tries")
    plt.savefig("/vagrant/xrdcp-test-plot.png")


if __name__ == '__main__':
    copy_try_counter  = 0
    for ip in CONST_DESTINATIONS:
        std_err_string = execute_xrdcp(FILE_TO_COPY, ip)
        failed = validate_result(std_err_string, ip)
        if not failed:
            remove_copy(FILE_TO_COPY)
        copy_try_counter += 1
    generate_bar_chart(copy_try_counter)
