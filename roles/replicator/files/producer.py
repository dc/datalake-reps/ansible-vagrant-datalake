from time import sleep
from json import dumps
from random import choice
from random import randrange
from string import ascii_uppercase
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['192.168.122.214:9092'], value_serializer=lambda x:
                         dumps(x).encode('utf-8'))

while True:
    randomFileName = ''.join(choice(ascii_uppercase) for i in range(7))
    replications = randrange(1, 6, 1)
    data = {"file": randomFileName, "replicationCount": replications, "rule": "rule1"}
    producer.send('replication', value=data)
    print("sent", data)
    sleep(10)
